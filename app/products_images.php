<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class products_images extends Model {
    
    protected $table ="products_images";
    protected $fillable = ['image'] ; 
    public $timestamps=true;
     public function products()
    {
        return $this->belongsTo('App\products');
    }
   
}
