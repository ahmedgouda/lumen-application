<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class products extends Model {
    
    protected $table ="products";
    protected $fillable = ['product_name','product_price'] ; 

    public function category()
    {
        return $this->belongsTo('App\products_category');
    }
     public function images()
    {
        return $this->hasMany('App\products_images');
    }
     public function discount()
    {
        return $this->belongsTo('App\product_discount');
    }
}
