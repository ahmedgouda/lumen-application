<?php

namespace App\Http\Middleware;

use Closure;

class discount_middleware
{
    
    public function handle($request, Closure $next)
    {
        
            $discount = $request->input("discount");
            $price =  $request->input("product_price");
        if($discount!=null){
            $new_price = $price -(($price*$discount)/100);
            $request->merge(array('product_price' => $new_price));

        }        
        return $next($request);
    }
}
