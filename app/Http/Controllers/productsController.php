<?php

namespace App\Http\Controllers;

use App\products;
use App\products_images;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

class productsController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    public function show($id) {

        if (Cache::has('product'.$id)) {
            return Cache::get('product'.$id);
        } else {
            $product = products::find($id);
            if ($product == null) {
                return response()->json(["status" => "fail", "error" => "this product doesn't exist", "product" => ""]);
            }
            $product->images = $this->get_product_images($id);
            Cache::put('product'.$id, $product, 10);
            return $product;
        }
    }

    public function delete($id) {
        $product = products::find($id);

        if ($product == null) {
            return response()->json(["status" => "fail", "error" => "this product doesn't exist", "product" => ""]);
        } else {
            $product_obj = $product;
            $product->delete();
            return response()->json(["status" => "success", "error" => "", "product" => $product_obj]);
        }
    }

    public function index() {

        if (Cache::has('products')) {
            return Cache::get('products');
        } else {
            $products = products::get();
            foreach ($products as $product) {
                $id = $product->id;
                $imges = $this->get_product_images($id);
                $product->images = $imges;
            }
            Cache::put('products', $products, 10);

            return $products;
        }
    }

    public function upload_files(Request $request) {

        $final_array = array();
        if ($request->has('images')) {
            $images = $request->file('images');

            for ($i = 0; $i < count($images); $i++) {
                $photoName = "-text.png";
                $image = $images[$i];
                $image_resize = Image::make($image->getRealPath());
                $image_resize->resize(175, 37);
                $photoName = rand(10, 100000) . time() . '.' . $image->getClientOriginalExtension();
                $image_resize->save('uploads/' . $photoName);
                $final_array[$i] = url() . '/uploads/' . $photoName;
            }
        }
        return $final_array;
    }

    public function store_images_to_product($images, $id) {
        for ($i = 0; $i < count($images); $i++) {
            $product_images = new products_images();
            $product_images->product_id = $id;
            $product_images->image = $images[$i];
            $product_images->save();
        }
    }

    public function get_product_images($product_id) {
        $product_images = products_images::where("product_id", $product_id)->get();
        $images_array = array();
        $i = 0;
        foreach ($product_images as $images) {
            $images_array[$i] = $images->image;
            $i++;
        }
        return $images_array;
    }

    public function store(Request $request) {
        $validator = \Validator::make($request->all(), [
                    'product_name' => 'required|unique:products',
                    'product_price' => 'required',
                    'product_code' => 'required',
                    'discount' => 'numeric|min:1|max:100',
                    'category_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(["status" => "fail", "errors" => $validator->errors(), "product" => ""], 422);
        }

        $product = new products();
        $product->product_name = $request->input("product_name");
        $product->product_price = $request->input("product_price");
        $product->product_code = $request->input("product_code");
        $product->discount = $request->input("discount");
        $product->category_id = $request->input("category_id");
        $category = \App\products_category::find($request->input("category_id"));
        if ($category == null) {
            $res = array(
                'status' => 'fail', 'errors' => 'category not exist', 'product' => ""
            );
            return response()->json([$res]);
        }
        $product->save();

        $images = $this->upload_files($request);
        $this->store_images_to_product($images, $product->id);
        $product->images = $images;
        $res = array(
            'status' => 'success', 'errors' => '', 'product' => $product
        );
        return $res;
    }

    //
}
