<?php

namespace App\Http\Controllers;
use App\products_category;
use Illuminate\Http\Request;
class products_categoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }
    public function show($id)
    {
        return products_category::findOrFail($id);
    }
     public function delete($id)
    {
        $category= products_category::find($id);
        
        if($category==null){
            return response()->json(["status"=>"fail","error"=>"this category doesn't exist","category"=>""]);
        }else {
        $category_name = $category->product_name;
            $category->delete();
            return response()->json(["status"=>"success","error"=>"","product"=>$category_name]);
        }
         
    }
    public function index(){
        return products_category::get();
    }
    public function store(Request $request){
        $validator = \Validator::make($request->all(),[
        'category_name' => 'required|unique:products_category',
                ]); 
        if ($validator->fails()) {
   return response()->json(["status"=>"fail","errors"=>$validator->errors(),"category"=>""], 422);
}

        $category = new products_category();
        $category->category_name = $request->input("category_name");
        $category->save();
         $res=array(
             'status' => 'success','errors' =>'','product'=>$category
         );
         return response()->json([$res]);
    }
   
   

    //
}
