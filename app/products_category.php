<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class products_category extends Model {
    
    protected $table ="products_category";
    public $timestamps=true;
    public function products()
    {
        return $this->hasMany('App\products');
    }
   
}
