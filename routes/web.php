<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
//Route::group(['middleware'=> 'discount_middleware'],function(){ 
$router->get('product/{id}', 'productsController@show');
$router->get('product', 'productsController@index');

$router->post('product/insert', 'productsController@store');

$router->delete('product/{id}', 'productsController@delete');


$router->get('category/{id}', 'products_categoryController@show');
$router->post('category/insert', 'products_categoryController@store');
$router->get('category', 'products_categoryController@index');
$router->delete('category/{id}', 'products_categoryController@delete');

