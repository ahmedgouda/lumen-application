<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Constraints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products',function(Blueprint $table){
             $table->unsignedInteger('category_id');
         $table->foreign('category_id')->references('id')->on('products_category')->onDelete('cascade');
        });
        Schema::table('products_images',function(Blueprint $table){
             $table->unsignedInteger('product_id');
         $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
